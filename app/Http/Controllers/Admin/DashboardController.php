<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
    */
    public function index(){
        return view('home');
    }

    /**
     * Show the user list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
    */
    public function user(){
        $users = User::withTrashed()->where('user_type', 2)->paginate(10);
        return view('users.index', compact('users'));
    }


    /**
     * Change status of user
     *  @param \Illuminate\Http\Request $request
     *
     * @return $user
    */
    public function change(Request $request){

        $user = User::findOrFail($request->id);
        $status =  ($request->status == 1) ? '2' : '1';
        $user->update(['status' => $status]);

        return $user;
    }

    /**
     * Delete user
     *  @param \Illuminate\Http\Request $request
     *
     * @return $user
    */
    public function delete(Request $request){
        $user = User::findOrFail($request->id);
        $user->delete();

        return $user;
    }

    /**
     * Restores user
     *  @param \Illuminate\Http\Request $request
     *
     * @return $user
    */
    public function restore(Request $request){
        $user =User::withTrashed()->find($request->id);
        $user->restore();
        return $user;
    }
}