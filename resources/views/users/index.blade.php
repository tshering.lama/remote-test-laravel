@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Manage Users</div>
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>User Type</th>
                            <th>Status</th>
                            <th>Deleted At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($users as $key => $value)
                            <tr>
                                <th>{{ $value->id }}</th>
                                <th>{{ $value->name }}</th>
                                <th>{{ $value->email  }}</th>
                                <th>{{ $value->user_type_html }}</th>
                                <th>{!! $value->user_status !!}</th>
                                <th>{{ $value->deleted_at }}</th>
                                <th>

                                    @if($value->deleted_at)
                                    <a href="#" id="restore" class="btn btn-success restore" data-id="{{ $value->id }}">Restore</a>
                                    @else
                                        <a href="#" id="change_status" data-id="{{ $value->id }}" data-status="{{ $value->status }}" class="btn btn-primary change_status">Mark {{ ($value->status == 1) ? 'Inactive' : 'Active'  }}</a>&nbsp;||&nbsp;
                                        <a href="#" id="delete" class="btn btn-danger delete" data-id="{{ $value->id }}">Delete</a>
                                    @endif
                                </th>
                            </tr>
                        @empty
                            <tr colspan="6">No data found</tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <script>
        $('.change_status').click(function(event) {
            event.preventDefault();
            var change_url = "{!! route('user.change.status') !!}";
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');
            $.ajax({
                type:'POST',
                url: change_url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    id: id,
                    status: status

                },
                success: function(response) {
                    alert('Status changed successfully');
                    location.reload(true);
                },
                error: function(xhr) {
                    alert('something went wrong')
                }
            });
        });

        $('.delete').click(function(event) {
            event.preventDefault();
            var change_url = "{!! route('user.delete') !!}";
            var id = $(this).attr('data-id');
            $.ajax({
                type:'POST',
                url: change_url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    id: id

                },
                success: function(response) {
                    alert('Data deleted successfully');
                    location.reload(true);
                },
                error: function(xhr) {
                    alert('something went wrong')
                }
            });
        });

        $('.restore').click(function(event) {
            event.preventDefault();
            var change_url = "{!! route('user.restore') !!}";
            var id = $(this).attr('data-id');
            $.ajax({
                type:'POST',
                url: change_url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    id: id

                },
                success: function(response) {
                    alert('Data restored successfully');
                    location.reload(true);
                },
                error: function(xhr) {
                    alert('something went wrong')
                }
            });
        });
    </script>
@endpush
