<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\User\UserDashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();
Route::group(['prefix' => 'dashboard/admin', 'middleware' => 'IsAdmin'], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('/users', [DashboardController::class, 'user'])->name('admin.dashboard.user');
    Route::post('/user/change/status', [DashboardController::class, 'change'])->name('user.change.status');
    Route::post('/user/delete', [DashboardController::class, 'delete'])->name('user.delete');
    Route::post('/user/restore', [DashboardController::class, 'restore'])->name('user.restore');
});


Route::group(['prefix' => 'dashboard/user', 'middleware' => 'IsUser'], function () {
    Route::get('/', [UserDashboardController::class, 'index'])->name('user.dashboard');

});