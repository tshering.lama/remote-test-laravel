<?php

namespace App\Http\Controllers\Auth;

use Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
       if (Auth::check()) {
            if($user->user_type ==1){

                return redirect()->route('admin.dashboard');

            }elseif($user->user_type ==2 && $user->status == 1){

                return redirect()->route('user.dashboard');

            }else{
                Session::flush();
                return Redirect::to('/login');
            }
        }

    }
}