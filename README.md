## Remote Coach Test

# Clone the project

# cd remote-test-laravel

# composer install

# cp .env.example .env

# change database credentials

# php artisan:migrate --seed

# npm install && npm run dev

# php artisan key:generate

# php artisan serve

# login with credentails

email: superadmin@site.com
password: password
